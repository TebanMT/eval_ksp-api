# API Evaluacion KSP

## Description :book:
En este repositorio se encuentra la API REST, desarrollada con flask, para la evaluación tencnica de la empresa KSP.
El proyecto cuenta con un Make faile con comandos muy utiles.


## Tecnologias Utilizadas
- python3
- flask     -- **web framework**
- gunicorn  -- **servidor HTTP** 
- honcho    -- **manejador de procfile**
- nosetest  -- **formato a las pruebas unitarias y covertura**
- Docker    -- **contenedor**
- Docker Compose    -- **Orquestador**
- Nginx     -- **Servidor web**
- postgres  -- **Base de datos**

## Instalación :hammer:
### Desarrollo
Si se desea instalar el proyecto en un ambiente de desarrollo, se recomienda crear un entorno virtual ('make env' lo hace por usted :) )
y una vez creado installar las dependencias descritas en el archivo requirements.txt (make install).

La opción recomendada es utilizar el servidor de flask para desarrollo dado que aun no existe un dokcer-compose para este fin.
Sin embargo el make file ya tiene los comandos necesarios para levantarlo. Los siguientes comandos son para levantar el proyecto en su maquina local:

```
$ make install      // Instalar requerimientos
$ make db           // Levantar una instancia de postgres
$ make tests        // Verificar pruebas unitarias con nosetests :)
$ make lint         // (opcional) ejecutar linters
$ make run_flask    // Iniciar el proyecto con el servidor de flask en el puerto http://127.0.0.1:8000


// Si desea iniciar con honcho
$ make run
```

Si desea levantar una instancia integrada de la aplicacion, puede hacerlo con docker-compose; siguientes commandos:
```
$ docker-compose up     // Levantar los servicios y verificar logs
$ docker-compose up -d  // Modo detach (remplazo del comando anterior si no desea ver logs)

// Esto levantara una instancia de postgres, la app y nginx. Nginx esta configurado para exponer el puerto 1337
```
Tener en cuenta que si desea hacer cambios en este modo, es necesario modificar 'tebanmt/employes:1.0.0' en el docker compose por sus imagenes que vaya creando.

### Producción
Para desplegar en producción es necesario tener la ultima version de la imagen en el registry de docker.
Actualmente la aplicación esta deplegada en un drop de DigitalOcean: http://165.232.132.62:1337/


# Estructura :file_folder:
```
root
│   README.md
│   requeriments.txt
|   Dockerfile
|   Makefile    // Comandos  
|   Procfile   // gunicorn execution
|   docker-compose.yaml
|   setup.cfg  //linters y nosetests config
|  
└───nginx
    |   Dockerfile
    |   nginx.conf  // configuracion nginx 
    |
    └───service
    │       __init__.py //Init App
    │       config.py  // config database variables
    |       models.py // Modelos de base de datos
    |       routes.py // Rutas REST (controlador)
    │_______common
    |           cli_commands.py
    |           error_handlers.py
    |           log_handlers.py
    |           status.py   // status http
    |           util.py     // subir img a s3
    │    
    └───tests
    │       __init__.py
    |       factories.py    // object factories
    |       test_cli_commands.py
    |       test_models.py
    |       test_routes.py
```

## TODOS

- [ ] Agregar mayor cobertura en pruebas
- [ ] Agregar certificado SSL para https
- [ ] Agregar secrets para las configuraciones
- [ ] Mover repositorio a Github para utilizar Github Actions
- [ ] Agregar pipeline en github actions para CI/CD
- [ ] Utilizar Zenhub para gestionar tareas historias de usuario
- [ ] Actualizar unicamente beneficiarios
- [ ] Agregar tablas de catalogos a la bd (genero, status)
- [ ] Agregar docker para desarrollo (con livereload)
- [ ] Utilizar Kubernetes
- [ ] Cambiar este readme a ingles
